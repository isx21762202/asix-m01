# Javier Moyano Vejarano
# HISX1 isx21762202
# Sistemes Operatius

TASCA 6


System wide:
- /etc/profile
- /etc/bashrc

Personalització d'usuari
- .bash_profile
- .bashrc

Variables d'entorn i startup programs
- /etc/profile
- .bash_profile

Funcions i àlies
- /etc/bashrc
- .bashrc
