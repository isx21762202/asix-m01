#! /bin/bash
# Javier Moyano Vejarano isx21762202
# Escola del Treball 2019-2020
# HISX1
# ----------------------------------
# crea-classe--file.sh curs fitxer

OK=0
ERR_NARGS=1
ERR_FILE=2

if [ $# -ne 2 ]; then
 echo "ERROR: nombre d'arguments erroni."
 echo "Usage: $0 curs fitxer"
 exit $ERR_NARGS
fi

curs=$1
file=$2

if [ ! -f $file ];then
  echo "ERROR: $file no és un arxiu."
  echo "Usage: $0 curs file"
  exit $ERR_FILE
fi

groupadd $curs &> /dev/null
while read -r nom
do
  usuari=$(echo $nom | cut -d: -f1)
  contrasenya=$(echo $nom | cut -d: -f2)

  mkdir /home/$curs 2> /dev/null
  useradd $usuari -g $curs -G users -m -b /home/$curs/
  echo "$usuari:$contrasenya" | chpasswd
done < $file
exit $OK


