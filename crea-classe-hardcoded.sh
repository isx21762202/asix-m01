#! /bin/bash
# Javier Moyano Vejarano isx21762202
# Escola del treball 2019 - 2020
# HISX1
# -----------------------------------
# crea-classe-hardcoded curs

OK=0
ERR_NARGS=1

if [ $# -ne 1 ];then
  echo "ERROR: nombre d'arguments erroni."
  echo "Usage: $0 group "
  exit $ERR_NARGS
fi

curs=$1
alumnes=$(echo $curs-{01..30})

groupadd $curs &> /dev/null
for alumne in $alumnes
do
  mkdir /home/$curs 2> /dev/null
  useradd $alumne -g $curs -G users -m -b /home/$curs/
  echo "$alumne:alum" | chpasswd
done
exit $OK
