#! /bin/bash
# Javier Moyano Vejarano isx21762202
# Escola del treball 2019 - 2020
# HISX1
# -----------------------------------
# crea-classe-random curs alumne[...]

OK=0
ERR_NARGS=1

if [ $# -lt 2 ];then
  echo "ERROR: nombre d'arguments erroni."
  echo "Usage: $0 curs alumne{...} "
  exit $ERR_NARGS
fi

curs=$1
shift

for alumne in $*
do
  groupadd $curs &> /dev/null
  mkdir /home/$curs 2> /dev/null
  useradd $alumne -g $curs -G users -m -b /home/$curs/
  contrasenya=$(pwgen -y 8 1)
  echo "$alumne:$contrasenya" | chpasswd
  echo "$alumne:$contrasenya" >> ./passwordd.log
done
exit $OK
