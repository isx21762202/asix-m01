#! /bin/bash
# Javier Moyano Vejarano isx21762202
# Escola del treball 2019-2020
# HISX1
# ----------------------------------
# del_usuari login
# Desa tots els fitxers que pertanyen a l'usuari a un tar.gz i elimina TOT el que li pertany. Tot el que s'esborra surt per stderr.

OK=0
ERR_NARGS=1
ERR_LOGIN=2
ERR_TAR=3
ERR_USERDEL=4

#Validem els arguments
if [ $# -ne 1 ]; then
  echo "ERROR: nombre d'arguments incorrecte."
  echo "Usage: $0 login"
  exit $ERR_NARGS
fi

userexist=""
userexist="$(grep "^$1:" /etc/passwd)"

# Validem el login
if [ -z $userexist ]; then
  echo "ERROR: l'usuari $1 no existeix."
  echo "Usage: $0 login"
  exit $ERR_LOGIN
fi

usuari=$1

# Eliminem tot el que pertany a l'usuari i fem el tar dels seus fitxers.

lprm -U $usuari &> /dev/null
echo "Treballs d'impresió de $usuari esborrats." >> /dev/stderr

crontab -u $usuari -r &> /dev/null
echo "Tasques periòdiques de $usuari esborrades" >> /dev/stderr

killall --user $usuari &> /dev/null 
echo "Processos de $usuari eliminats." >> /dev/stderr

fitxersusuari=$(find / -user $usuari -type f 2> /dev/null)

tar -czf /tmp/backup$usuari.tar $fitxerusuari &> /dev/null
if [ $? -ne 0 ]; then
  echo "ERROR: no ha sigut possible comprimir els arxius."
  exit $ERR_TAR
fi

userdel -r $usuari &> /dev/null
echo "Usuari $usuari esborrat."

exit $OK
