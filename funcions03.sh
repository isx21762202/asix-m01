#! /bin/bash
# Javier Moyano Vejarano
# HISX1 isx21762202
# Escola del Treball 2019-2020 
# ----------------------------------------------
# 9. Llistar per ordre de gname tots els grups del sistema, per a cada grup hi ha una capçalera amb el nom del grup i a continuació el llistat de tots els usuaris que
# tenen aquell grup com a grup principal, ordenat per login.

function showAllGroupMainMembers(){
	
gnamesiGID=$(cut -d: -f1,3 /etc/group | sort -u)
for grup in $gnamesiGID
do
  gname=$(echo $grup | cut -d: -f1)
  GID=$(echo $grup | cut -d: -f2)
  echo "$gname"
  grep "^[^:]*:[^:]*:[^:]*:$GID:" /etc/passwd | cut -d: -f1 | sort | sed -r "s/^(.*)$/\t\1/g" 
done
return 0
}

# 11. Fer una nova versió de showAllGroupMembers on per cada grup es llisti una capçalera amb el nom del grup i la quantitat d’usuaris que tenen aquest grup com a grup
# principal. Per a cada grup llavors es llisten les línies de detall dels usuaris que hi pertanyem, per ordre de login, mostrant login, uid, home i shell.

function showAllGroupMainMembers2(){

contadorusuaris=0
grupsiGID=$(cut -d: -f1,3 /etc/group | sort -u )

for group in $grupsiGID
do
  gname=$(echo $group | cut -d: -f1)
  GID=$(echo $group | cut -d: -f2)
  num=$(grep "^[^:]*:[^:]*:[^:]*:$GID:" /etc/passwd | wc -l)
  echo "$gname $(echo "$num")"
  grep "^[^:]*:[^:]*:[^:]*:$GID:" /etc/passwd | cut -d: -f1,3,5,6 | sed -r "s/^(.*)$/\t\1/g"| tr ":" " " | sort -t" " -k1
  echo ""
done
}

# 18. Processa un fitxer de text que conté un gid per línia o bé stdin (on també espera rebre un gid per línia). Mostra el llistat dels usuaris (login, uid, home) d’aquells usuaris que tenen aquest grup com a grup principal.

function showGidMembers(){

OK=0
ERR_NARGS=1
ERR_FILE=2

fileIn=/dev/stdin
if [ $# -gt 1 ];then
  echo "ERROR: Nombre d'arguments erroni"
  echo "Usage: $0 file 'or' $0"
  return $ERR_NARGS
fi

if [ $# -eq 1 ];then
  if [ ! -f $1 ];then
    echo "ERROR: $1 no és un file."
    echo "Usage: $0 [file]."
    return $ERR_FILE
  fi
  fileIn=$1
fi

while read -r GID
do
  grep "^[^:]*:[^:]*:[^:]*:$GID:" /etc/passwd | cut -d: -f1,3,6 | sed -r "s/^(.*)$/\t\1/g" | tr ":" "\t" | sort 
done < $fileIn
return $OK
}

# 19. Processa un fitxer de text que conté un gid per línia o bé stdin (on també espera rebre un gid per línia). Mostra el llistat dels usuaris (login, uid, home) d’aquells usuaris que tenen aquest grup com a grup principal. Fa un having, mostrant només els grups si hi ha almenys 3 usuaris al grup.

function showGidMembers2(){

OK=0
ERR_NARGS=1
ERR_FILE=2

fileIn=/dev/stdin
if [ $# -gt 1 ];then
  echo "ERROR: Nombre d'arguments erroni"
  echo "Usage: $0 file"
  echo "Usage: $0"
  return $ERR_NARGS
fi

if [ $# -eq 1 ];then
  if [ ! -f $1 ];then
    echo "ERROR: $1 no és un file."
    echo "Usage: $0 [file]."
    return $ERR_FILE
  fi
  fileIn=$1
fi

while read -r GID
do
  usuaris=$(grep "^[^:]*:[^:]*:[^:]*:$GID:" /etc/passwd | cut -d: -f1,3,6 | sed -r "s/^(.*)$/\t\1/g" | tr ":" "\t" | sort )
  quantitatusuaris=$(echo "$usuaris" | wc -l)
  if [ $quantitatusuaris -ge 3 ];then
    echo "GID: $GID"
    echo "$usuaris"
  fi
done < $fileIn
return $OK
}

# 20. Donat un número d’oficina com a argument (validar es rep un argument i que és un número d’oficina vàlid), obtenir els codis dels representants de vendes que treballen en aquesta oficina i llistar les comandes d’aquest repventas. És a dir, estem fent un llistat de les comandes agrupades per venedors dels venedors que treballen en una determinada oficina.

function showPedidos(){

OK=0
ERR_NARGS=1
ERR_OFICINA=2

if [ $# -ne 1 ];then
  echo "ERROR: nombre erroni d'arguments."
  echo "Usage: $0 oficina."
  return $ERR_NARGS
fi

oficina=$1
egrep "^$oficina[[:blank:]]" ./oficinas.dat &> /dev/null
if [ $? -ne 0 ]; then
  echo "ERROR: $oficina no és una oficina."
  echo "Usage: $0 oficina"
  return $ERR_OFICINA
fi

codiempleats=$(tr '\t' ':' < ./repventas.dat | egrep "^[^:]*:[^:]*:[^:]*:$oficina" | cut -d: -f1)
for representant in $codiempleats
do
  tr '\t' ':' < ./pedidos.dat | egrep "^[^:]*:[^:]*:[^:]*:$representant" | tr ':' '\t'
done
return 0
}

# 21. getHome login . Fer una funció que rep un login (no cal validar que rep un argument ni que sigui un login vàlid) i mostra per stdout el home de l’usuari. La funció retorna 0 si ha pogut trobar el home i diferent de zero si no l’ha pogut trobar.

function getHome(){

OK=0
ERR_NOLOGIN=3

Login=$1
homedir=""
homedir=$(egrep "^$Login:" /etc/passwd | cut -d: -f6 2> /dev/null)
if [ -z $homedir ];then
  return $ERR_NOLOGIN
else
  echo "$homedir"
  return $OK
fi
}

# 22. .getHoleList login[...] . Fer una funció que rep un o més login d’arguments i per a cada un d’ells mostra el seu home utilitzant la funció getHome. Cal validar els arguments rebuts.

function getHomeList(){
OK=0
ERR_NARG=1

if [ $# -lt 1 ];then
  echo "ERROR: Nombre d'arguments erroni."
  echo "Usage: $0 login[...]"
  return $ERR_NARG
fi

for login in $*
do
  linia=""
  linia=$(grep "^$login:" /etc/passwd)
  if [ -z $linia ];then
    echo "ERROR: El login $login no existeix." >> /dev/stderr
  else
    getHome $login
  fi
done
return $OK
}

# 23. getSize homedir . Donat un homedir com a argument mostra per stdout l’ocupació en bytes del directori. Per calcular l’ocupació utilitza du del que només volem el resultat numèric. Cal validar que el directori físic existeix (recordeu que hi ha usuaris que poden tenir de home valors que no són rutes vàlides com /bin/false). Retorna 0 si el directori existeix i un valor diferent si no existeix. Cal usar les funcions que estem creant.

function getSize(){

OK=0
DIR_NOEXIST=1

HomeDir=$1
if [ -d $HomeDir ];then
  ocupacio=$(du -sh $HomeDir | cut -f1)
  echo "$HomeDir ocupa $ocupacio"
  return $OK
else
  echo "$HomeDir no és un directori" > /dev/stderr
  return $DIR_NOEXIST
fi
}

# 24. getSizeIn . Ídem exercici anterior però processa un a un els login que rep per stdin, cada línia un lògin. Valida que el login existeixi, si no és un error. Donat el login mostra per stdout el size del seu home. Cal usar les funcions que estem definint.

function getSizeIn(){

Stdin=/dev/stdin

while read -r login
do
  Linia=""
  Linia=$(grep "^$login:" /etc/passwd 2> /dev/null) 
  if [ -z $Linia ];then
    echo "El login $login no existeix." > /dev/stderr
  else
    getHome $login > /dev/null
    getSize $homedir
  fi
  echo "--------------------------"
done < $Stdin
}

# 25. getAllUsersSize . Processa una a una les línies del fitxer /etc/passwd i per a cada usuari mostra per stdout el size del seu home.

function getAllUsersSize(){

fitxer=/etc/passwd
while read -r line
do
  Login=$(echo $line | cut -d: -f1)
  echo "$Login"
  getHome $Login > /dev/null
  getSize $homedir
echo "-----------------------"
done < $fitxer
return 0
}
  
