#! /bin/bash
# Javier Moyano Vejarano
# HISX1 isx21762202
# Escola del Treball 2019-2020 
# ----------------------------------------------

# 1. Mesurar el du del home d'un login. El home es treu de /etc/passwd.
# fsize arg

function fsize(){

Login=$1
Home=$(grep "^$Login:" /etc/passwd | cut -d: -f6)
if ! [ -d $Home ];then
  echo "ERROR: El home $Home de $Login no és un directori vàlid." >> /dev/stderr
fi

du -sh $Home 2> /dev/null
}

# 2. Rep logins, i per a cada login es mostra l'ocupació de disc del home de l'usuari usant fsize. Verificar que es rep almenys un argument. Per a cada argument verificar si es un login vàlid, sino genera uan traça d'error.
# loginargs login[...]

function loginargs(){

OK=0
ERR_NARGS=1

if [ $# -lt 1 ];then
  echo "ERROR: nombre d'arguments no vàlid." >> /dev/stderr
  echo "Usage: $0 login[...]" >> /dev/stderr
  return $ERR_NARGS
fi

for login in $*
do
  linia=""
  linia=$(grep "^$login:" /etc/passwd)
  if [ -z '$linia' ];then
    echo "ERROR: $login no és un login vàlid." >> /dev/stderr
  else
    fsize $login
  fi
done
return $OK
}

# 3. Rep com a argument un nom de fitxer que conté un login per línia. Mostrar l'ocupació de disc de cada usuari usant fsize. verificar que es rep un arguemnt i que es un regular file.
# loginfile file

function loginfile(){
  
OK=0
ERR_NARGS=1
ERR_NOFILE=2

if [ $# -ne 1 ];then
  echo "ERROR: nombre d'arguments no vàlid." >> /dev/stderr
  echo "Usage: $0 file" >> /dev/stderr
  return $ERR_NARGS
fi

file=$1
if ! [ -f $file ];then
  echo "ERROR: $file no és un file." >> /dev/stderr
  echo "Usage: $0 file" >> /dev/stderr
  return $ERR_NOFILE
fi

while read -r usuari
do
  linia=""
  linia=$(grep "^$usuari:" /etc/passwd)
  if [ -z '$linia' ];then
    echo "ERROR: $usuari no és un login vàlid." >> /dev/stderr
  else
    fsize $usuari
  fi
done < $file
return $OK
}

# 4. Rep com a argument un file o res (en aquest cas es processa stdin). El fitxer o stdin contenen un lògin per línia. Mostrar l'ocupació de disc del home de l'usuari. Verificar els arguments rebuts. verificar per cada login rebut que és vàlid.
# loginboth [file]

function loginboth(){
  
OK=0
ERR_NARGS=1
ERR_NOFILE=2

if [ $# -gt 1 ];then
  echo "ERROR: nombre d'arguments no vàlid." >> /dev/stderr
  echo "Usage: $0 [file]" >> /dev/stderr
  return $ERR_NARGS
fi

stdin=/dev/stdin
if [ $# -eq 1 ];then
  file=$1
  if ! [ -f $file ];then
    echo "ERROR: $file no és un file." >> /dev/stderr
    echo "Usage: $0 file" >> /dev/stderr
    return $ERR_NOFILE
  fi
  stdin=$file
fi

while read -r Usuari
do
  linia=""
  linia=$(grep "^$Usuari:" /etc/passwd)
  if [ -z '$linia' ];then
    echo "ERROR: $Usuari no és un login vàlid." >> /dev/stderr
  else
    fsize $Usuari
  fi
done < $stdin
return $OK
}

# 5. Donat un GID com a argument, llistar els logins dels usuaris que pertanyen a aquest grup com a grup principal. Verificar que es rep un argument i que és un GID vàlid.
# grepgid GID

function grepgid(){

OK=0
ERR_NARGS=1
ERR_NOGID=2

if [ $# -ne 1 ];then
  echo "ERROR: nombre d'arguments no vàlid." >> /dev/stderr
  echo "Usage: $0 GID" >> /dev/stderr
  return $ERR_NARGS
fi

gid=$1
linia=""
linia=$(grep "^[^:]*:[^:]*:$gid:" /etc/group)
if [ -z '$linia' ];then
  echo "ERROR: $gid no és un GID vàlid." >> /dev/stderr
  echo "Usage: $0 GID" >> /dev/stderr
  return $ERR_NOGID
fi

grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1

return $OK
}

# 6. Donat un GID com a argument, mostrar per a cada usuari que pertany a aquest grup l'ocupació de disc del seu home. Verificar que es rep un argument i que és un GID vàlid.
# gidsize GID

function gidsize(){

OK=0
ERR_NARGS=1
ERR_NOGID=2

if [ $# -ne 1 ];then
  echo "ERROR: nombre d'arguments no vàlid." >> /dev/stderr
  echo "Usage: $0 GID" >> /dev/stderr
  return $ERR_NARGS
fi

gid=$1
linia=""
linia=$(grep "^[^:]*:[^:]*:$gid:" /etc/group)
if [ -z '$linia' ];then
  echo "ERROR: $gid no és un GID vàlid." >> /dev/stderr
  echo "Usage: $0 GID" >> /dev/stderr
  return $ERR_NOGID
fi

USuaris=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1)

loginargs $USuaris 2> /dev/null

return $OK
}

# 7. Llistar de tots els GID del sistema (en ordre creixent) l'ocupació del home dels usuaris que hi pertanyen.
# allgidsize

function allgidsize(){
  
OK=0

gids=$(cat /etc/group | cut -d: -f3 | sort -k1g)
for grup in $gids
do
  echo $grup
  gidsize $grup
  echo ""
done

return $OK
}

# 8. Llistar totes les línies de /etc/group i per cada llínia llistar l'ocupació del home dels usuaris que hi pertanyen. Ampliar filtrant només els grups del 0 al 100.
# allgroupsize

function allgroupsize(){
  
OK=0

grups=$(cat /etc/group)
for grupo in $grups
do
  echo $grupo
  usuarios=$(echo $grupo | cut -d: -f4 | tr "," " ")
  loginargs $usuarios 2> /dev/null
  echo ""
done

return $OK
}

# 9. Donat un fstype llista el device i el mountpoint (per odre de device) de les entrades de fstab d'quest fstype.
# fstype fstype

function fstype(){
  
OK=0

Fstype=$1
devimountp=""
devimountp=$(grep -v "^#" /etc/fstab | grep -v "^$" | tr -s "[[:blank:]]" " " | grep "^[^ ]* [^ ]* $Fstype " | cut -d' ' -f1,2 | sort )

if ! [ -z '$devimountp' ];then
  echo $devimountp
fi

return $OK
}

# 10. LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic) les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint.
# allfstype

function allfstype(){
  
OK=0

fstypes=$(grep -v "^#" /etc/fstab | grep -v "^$" | tr -s "[[:blank:]]" " " | cut -d' ' -f3 | sort -u )

for type in $fstypes
do
  echo $type
  ordre=$(fstype $type)
  echo "  $ordre" 
  echo ""
done

return $OK
}

# 11. LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic) les entrades d'quest tipus. 
# Llistar tabuladament el device i el mountpoint. Es rep un valor numèric d'argument que indica el numéro mínim d'entrades d'aquest fstype que hi ha d'haver per sortir al llistat.
# allfstypeif

function allfstypeif(){
  
OK=0
ERR_NARGS=1

if [ $# -ne 1 ];then
  echo "ERROR: nombre d'arguments no vàlid." >> /dev/stderr
  echo "Usage: $0 nombre_d'entrades_minim"  >> /dev/stderr
  return $ERR_NARGS
fi

entrades=$1
fstypes=$(grep -v "^#" /etc/fstab | grep -v "^$" | tr -s "[[:blank:]]" " " | cut -d' ' -f3 | sort -u )

for type in $fstypes
do
  ordre=$(fstype $type)
  if [ $(echo $ordre | wc -l) -ge $entrades ];then
    echo $type
    echo "  $ordre" 
    echo ""
  fi
done

return $OK
}
