#! /bin/bash
# Javier Moyano Vejarano | isx21762202
# 07/04/2020  

# Exercicis 27, 28, 29 de /etc/shadow


# 26. showShadow login
# Donat un login mostrar els camps del /etc/shadow d’aquest login, mostrant camp a camp amb una etiqueta i el valor del camp. Validar que es rep un argument i que el login existeix.

function showShadow(){
  
OK=0
ERR_NARGS=1
ERR_LOGIN=2

if [ $# -ne 1 ]; then
  echo "ERROR: nombre d'arguments erroni"
  echo "Usage: $0 login"
  echo ""
  return $ERR_NARGS
fi

liniaLogin=$(grep "^$1:" /etc/shadow)

if [ -z $liniaLogin ];then
  echo "ERROR: login $1 incorrecte."
  echo "Usage: $0 login."
  echo ""
  return $ERR_LOGIN
fi
  Loginn=$(echo $liniaLogin | cut -d: -f1)
  Contrasenya=$(echo $liniaLogin | cut -d: -f2)
  LastPasswordChange=$(echo $liniaLogin | cut -d: -f3)
  MinimunDaysToChangePasswd=$(echo $liniaLogin | cut -d: -f4)
  MaximumDaysToChangePasswd=$(echo $liniaLogin | cut -d: -f5)
  WarningDaysBeforePasswdExpires=$(echo $liniaLogin | cut -d: -f6)
  PasswdInactive=$(echo $liniaLogin | cut -d: -f7)
  AccountExpires=$(echo $liniaLogin | cut -d: -f8)
  
  echo "Login: $Loginn"
  echo "	Contrasenya: $Contrasenya"
  echo "	Último cambio de la contraseña: $LastPasswordChange"
  echo "	Mínimo de días para cambiar la contraseña: $MinimunDaysToChangePasswd"
  echo "	Máximo de días para cambiar la cotnraseña: $MaximumDaysToChangePasswd"
  echo "	Aviso de días antes de que expire la contraseña: $WarningDaysBeforePasswdExpires"
  echo "	Días hasta que la contraseña quede inactiva: $PasswdInactive"
  echo "	Días hasta que la cuenta quede inactiva: $AccountExpires"
  echo ""
  
return $OK
}


# ----------------------------------------------------------------------
# 27 showShadowList login[...]
# Mostrar per a cada lògin les dades del seu shadow. Validar que es rep almenys un argument i que existeix cada un dels lògins.

function showShadowList(){
  
OK=0
ERR_NARGS=1
ERR_LOGIN=2

if [ $# -lt 1 ]; then
  echo "ERROR: nombre d'arguments erroni."
  echo "Usage: $0 login[...]"
  return $ERR_NARGS
fi

for user in $*
do
  showShadow $user
done
return $OK
}


# ----------------------------------------------------------------------

# 28.showShadowIn < login
# Mostrar els camps idel shadow de cada usuari rebut per l’entrada estàndard. Cada línia de l’entrada estàndard correspon a un login. Verificar que cada un dels lògins existeix.

function showShadowIn() {

OK=0

stdIn=/dev/stdin

while read -r usuari
do
  showShadow $usuari
done < $stdIn

return $OK

}
